import 'package:app_special_production_erp_apis/model/contract_detail_item.dart';
import 'package:flutter/material.dart';

class ComponentWorkStatusItem extends StatelessWidget {
  const ComponentWorkStatusItem({super.key, required this.contractDetailItem
    // , required this.voidCallback
  });

  final ContractDetailItem contractDetailItem;
  // final VoidCallback voidCallback;

  @override
  Widget build(BuildContext context) {
    return _buildBody(context);
  }

  Widget _buildBody(BuildContext context) {
    if(contractDetailItem.endWorkDate == '-') {
      return GestureDetector(
        child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.black,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 130,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child:
                      Text('계약번호',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.white ,fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                      child:
                      Text('>> ${contractDetailItem.contractNumber}',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.white ,fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width -300,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('패키지명: ${contractDetailItem.packageName}', style: TextStyle(color: Colors.white)),
                    Text('현재 진행 공정명: ${contractDetailItem.presentProcessName}',style: TextStyle(color: Colors.white)),
                    Text('계약일자: ${contractDetailItem.contractDate}', style: TextStyle(color: Colors.white)),
                  ],
                ),
              ),
              Container(
                width: 60,
                color: Colors.grey,
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text('완료',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
              ),
            ],
          ),
        ),
      );
    } else {
      return GestureDetector(
        child: Container(
          margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
            color: Colors.black,
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                width: 130,
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child:
                      Text('계약번호',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.white ,fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 0, 0, 15),
                      child:
                      Text('>> ${contractDetailItem.contractNumber}',
                        textAlign: TextAlign.left,
                        style: TextStyle(color: Colors.white ,fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width -300,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('패키지명: ${contractDetailItem.packageName}', style: TextStyle(color: Colors.white)),
                    Text('현재 진행 공정명: ${contractDetailItem.presentProcessName}',style: TextStyle(color: Colors.white)),
                    Text('계약일자: ${contractDetailItem.contractDate}', style: TextStyle(color: Colors.white)),
                  ],
                ),
              ),
              Container(
                width: 60,
                color: Colors.deepOrange,
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                child: Text('완료',
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),),
              ),
            ],
          ),
        ),
      );
    }

  }
}
