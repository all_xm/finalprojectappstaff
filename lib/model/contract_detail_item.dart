class ContractDetailItem {
  String contractDate;
  String contractNumber;
  String customerName;
  String endWorkDate;
  String packageName;
  String presentProcessName;
  int presentProcessOrder;

  ContractDetailItem(this.contractDate, this.contractNumber, this.customerName, this.endWorkDate, this.packageName, this.presentProcessName, this.presentProcessOrder);

  factory ContractDetailItem.fromJson(Map<String, dynamic> json) {
    return ContractDetailItem(
      json['contractDate'],
      json['contractNumber'],
      json['customerName'],
      json['endWorkDate'],
      json['packageName'],
      json['presentProcessName'],
      json['presentProcessOrder'],
    );
  }

}