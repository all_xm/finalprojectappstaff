import 'package:app_special_production_erp_apis/pages/page_my.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _selectedIndex = 0;

  final List<BottomNavigationBarItem> _navItems = [
    const BottomNavigationBarItem(
      icon: Icon(Icons.person),
      label: '마이페이지',
    ),
  ];

  final List<Widget> _widgetPages = [
    const PageMy(),
  ];

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _widgetPages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _navItems,
        currentIndex: _selectedIndex,
        onTap: _onItemTap,
        selectedItemColor: Colors.blueAccent,
        unselectedItemColor: Colors.blueGrey,
      ),
    );
  }
}


// << Logout >>

// Future<void> _logout(BuildContext context) async {
  //   TokenLib.logout(context);
  //
  // }
  //
  // @override
  // Widget build(BuildContext context) {
  //   return Scaffold(
  //     body: Container(
  //       child: OutlinedButton(
  //         onPressed: () {
  //           _logout(context);
  //         },
  //         child: const Text('로그아웃'),
  //       ),
  //     ),
  //   );
  // }

