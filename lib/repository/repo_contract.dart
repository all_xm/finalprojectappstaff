import 'package:app_special_production_erp_apis/model/contract_detail_item.dart';
import 'package:app_special_production_erp_apis/model/contract_list_result.dart';
import 'package:dio/dio.dart';

import '../configs/config_api.dart';

class RepoContract {
  Future<ContractListResult> getList() async {
    const String baseUrl = '$apiUri/contract/all';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return ContractListResult.fromJson(response.data);
  }

  // Future<ContractListResult> getContractDetail(ContractDetailItem contractDetailItem) async {
  //   const String baseUrl = '$apiUri/contract/number';
  //
  //   Dio dio = Dio();
  //
  //   final response = await dio.get(
  //       baseUrl.replaceAll(contractDetailItem.contractNumber, contractDetailItem),
  //       options: Options(
  //           followRedirects: false,
  //           validateStatus: (status) {
  //             return status == 200;
  //           }
  //       )
  //   );
  //
  //   return ContractListResult.fromJson(response.data);
  // }

}