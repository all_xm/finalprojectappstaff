import 'package:app_special_production_erp_apis/functions/token_lib.dart';
import 'package:app_special_production_erp_apis/pages/page_index.dart';
import 'package:app_special_production_erp_apis/pages/page_login.dart';
import 'package:flutter/material.dart';

class MiddlewareLoginCheck { // middleware은 최대한 간단 명료하게 작성해야 함(메모리에 접근할 필요가 없음)
  void check(BuildContext context) async {
    int? memberId = await TokenLib.getMemberId();

    if (memberId == null) {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageLogin()), (route) => false);
    } else {
      Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (BuildContext context) => const PageIndex()), (route) => false);
    }
  }
}