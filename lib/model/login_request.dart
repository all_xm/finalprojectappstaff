class LoginRequest {
  String username;
  String password;

  LoginRequest(this.username, this.password);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>(); //final 이유 : 한 번 들어가면 변경되지 않을 것이므

    data['username'] = this.username;
    data['password'] = this.password;

    return data;
  }
}