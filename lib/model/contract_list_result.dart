import 'package:app_special_production_erp_apis/model/contract_detail_item.dart';

class ContractListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<ContractDetailItem> list;

  ContractListResult(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory ContractListResult.fromJson(Map<String, dynamic> json) {
    return ContractListResult(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => ContractDetailItem.fromJson(e)).toList()
    );
  }

}