import 'package:app_special_production_erp_apis/components/component_work_status_Item.dart';
import 'package:app_special_production_erp_apis/model/contract_detail_item.dart';
import 'package:app_special_production_erp_apis/repository/repo_contract.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

import '../components/common/component_custom_loading.dart';
import '../components/common/component_notification.dart';

class PageWorkStatus extends StatefulWidget {
  const PageWorkStatus({Key? key}) : super(key: key);

  @override
  State<PageWorkStatus> createState() => _PageWorkStatusState();
}

class _PageWorkStatusState extends State<PageWorkStatus> {
  List<ContractDetailItem> contractItems = [];

  Future<void> _getContractList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoContract().getList().then((res) {
      setState(() {
        contractItems = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  // Future<void> _getContractDetail(ContractDetailItem contractDetailItem) async {
  //   BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
  //     return ComponentCustomLoading(cancelFunc: cancelFunc);
  //   });
  //
  //   await RepoContract. {
  //     setState(() {
  //       contractItems = res.list;
  //     });
  //
  //     BotToast.closeAllLoading();
  //   }).catchError((err) {
  //     ComponentNotification(
  //       success: false,
  //       title: '데이터 로딩 실패',
  //       subTitle: '데이터 로딩에 실패하였습니다.',
  //     ).call();
  //
  //
  //     BotToast.closeAllLoading();
  //   });
  // }

  @override
  void initState() {
    super.initState();
    _getContractList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
    children: [
      for (int i = 0; i < contractItems.length; i++)
        ComponentWorkStatusItem(contractDetailItem: contractItems[i],
            // , voidCallback: _getContractDetail
      ),
    ],
    ),
    );
  }
}
