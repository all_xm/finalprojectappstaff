import 'package:app_special_production_erp_apis/middleware/middleware_login_check.dart';
import 'package:flutter/material.dart';

class LoginCheck extends StatefulWidget {
  const LoginCheck({Key? key}) : super(key: key);

  @override
  State<LoginCheck> createState() => _LoginCheckState();
}

class _LoginCheckState extends State<LoginCheck> {

  @override
  void initState() {
    super.initState();
    MiddlewareLoginCheck().check(context); // 로그인 되어있으면 check라는 곳에서 검사하고 멤버 id 없으면 로그인, 없으면 index페이지

  }

  @override
  Widget build(BuildContext context) {
    return Container();
  }
}
